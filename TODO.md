# TODO items

A list of tasks to do, following discussion with OpenIng partners, in
October 2022:

- Transformation of Sherpa/Romeo scripts into Django code for better
  integration within Dissemin and regular updating using a Dissemin task
- Debug Shibboleth authentication at Braunschweig/Stuttgart
- Debug the mangling of characters in Shibboleth names, see
  https://gitlab.com/dissemin/dissemin/-/issues/933
- Crossref import:
  * disable ElasticSearch input
  * only import things that have been recently updated
  * diagnose what takes time in the import to improve performance
- Base import:
  * turn the importing facilities from OaiPaperSource into something that
    we can use on a file -- the library for oai (oaimph) does support
    reading from a file, so just need to modify OaiPaperSource to allow
    using this feature from the library
  * Similar performance checks as Crossref
- Launch regular updating of Crossref and Base from their APIs
