# -*- mode: ruby -*-
# vi: set ft=ruby :

# All Vagrant configuration is done below. The "2" in Vagrant.configure
# configures the configuration version (we support older styles for
# backwards compatibility). Please don't change it unless you know what
# you're doing.
Vagrant.configure("2") do |config|
  # Load custom vbguest installer
  # The most common configuration options are documented and commented below.
  # For a complete reference, please see the online documentation at
  # https://docs.vagrantup.com.

  # Every Vagrant development environment requires a box. You can search for
  # boxes at https://atlas.hashicorp.com/search.
  config.vm.box = "debian/bullseye64"
  config.vm.box_version = "11.20211230.1"
#	config.vm.box = "debian/contrib-buster64"
#	config.vm.box_version = "10.7"

  # We want some more RAM. this is needed by ES and pdftk to run the tests
  config.vm.provider "virtualbox" do |v|
    v.memory = 1200
  end

  # We use VNU docker for some test (HTML-Validation)
  # Run it here
  #  config.vm.provision "docker" do |d|
  #    d.run "validator/validator:latest",
  #      args: "-it -p 8888:8888",
  #      daemonize: true
  #  end

  # Create a forwarded port mapping which allows access to a specific port
  # within the machine from a port on the host machine. In the example below,
  # accessing "localhost:8080" will access port 80 on the guest machine.
  config.vm.network "forwarded_port", guest: 8080, host: 8080

  # Enable provisioning with a shell script. Additional provisioners such as
  # Puppet, Chef, Ansible, Salt, and Docker are also available. Please see the
  # documentation for more information about their specific syntax and use.
  config.vm.provision "shell", path: "provisioning/provision.sh"

  # cf. https://github.com/dotless-de/vagrant-vbguest/issues/351 to
  # avoid issues in installing the Virtualbox guest additions
  if ENV['FIRST_RUN'] == 'true'
    config.vbguest.auto_update = false
    config.vm.synced_folder ".", "/dissemin", disabled: true
  else
    config.vm.synced_folder ".", "/dissemin", create: true, type: "virtualbox"
  end
end
