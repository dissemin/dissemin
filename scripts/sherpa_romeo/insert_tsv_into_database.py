#!/usr/bin/env python3

import psycopg2
import sys

assert len(sys.argv) == 3, "Usage: "+sys.argv[0]+" database password"

database = sys.argv[1]
password = sys.argv[2]

conn = psycopg2.connect(database=database, host="localhost",
        user="dissemin", password=password)
cur  = conn.cursor()

with open("pp.tsv", "r") as f:
    for line in f:
        line = line.rstrip("\n")
        fields = line.split("\t")
        cur.execute("SELECT 1 FROM papers_publisher WHERE romeo_id=%s FOR UPDATE", (fields[0],))
        if cur.fetchone() != None:
            cur.execute("""UPDATE papers_publisher
               SET name = %s,
                  alias = %s,
                    url = %s,
               preprint = %s,
              postprint = %s,
             pdfversion = %s,
              oa_status = %s,
           last_updated = %s WHERE romeo_id=%s""",
           tuple(fields[1:] + [fields[0]]))
        else:
            cur.execute(
            """INSERT INTO papers_publisher
                (romeo_id,name,alias,url,preprint,postprint,pdfversion,oa_status,last_updated)
               VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s)""", tuple(fields))

with open("journals.tsv", "r") as f:
    for line in f:
        line = line.rstrip("\n")
        fields = line.split("\t")

        # If there is no publisher policy, skip this journal
        if fields[5] == "":
            continue

        # Empty ISSN, ESSN should be NULLs
        for x in [2, 3]:
            if fields[x]=="":
                fields[x]=None
        
        cur.execute("SELECT id FROM papers_journal WHERE issn=%s OR essn=%s OR romeo_id=%s ORDER BY last_updated DESC FOR UPDATE", tuple(fields[2:4]+[fields[0]]))
        res = cur.fetchall()
        if len(res) > 0:
            if len(res)>1:
                # If more than one result, it means that a journal has
                # been split according to ISSN/ESSN or that some error in
                # the Sherpa/ROMEO caused a duplicate. In this case we
                # empty the ISSN/ESSN and the Romeo ID of the second match,
                # and just fill in the info for the first match; we could delete
                # the second entry as well, but it might be used by some
                # publications. We favor the most recent entry.
                for k in range(1,len(res)):
                    cur.execute("""UPDATE papers_journal
                                SET issn=NULL, essn=NULL, last_updated=%s, romeo_id=NULL
                                WHERE id=%s""", (fields[4],res[k][0]))
                    cur.execute("""UPDATE papers_oairecord SET journal_id=%s WHERE journal_id=%s""", (res[0][0], res[k][0]))

            cur.execute("""UPDATE papers_journal
               SET romeo_id = %s,
                      title = %s,
                       issn = %s,
                       essn = %s,
               last_updated = %s,
               publisher_id = (SELECT id FROM papers_publisher WHERE romeo_id=%s) WHERE id=%s""",
               tuple(fields + [res[0][0]]))
        else:
            cur.execute(
            """INSERT INTO papers_journal
              (romeo_id,title,issn,essn,last_updated,publisher_id)
              SELECT %s,%s,%s,%s,%s,id FROM papers_publisher WHERE romeo_id=%s""", tuple(fields))

conn.commit()
cur.close()
conn.close()
