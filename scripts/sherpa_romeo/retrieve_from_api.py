#!/usr/bin/env python3

import requests
import json
import os
import sys
import datetime
import urllib

assert len(sys.argv) == 2, "Usage: "+sys.argv[0]+" last_update_date_file.txt"
    
last_update_file = sys.argv[1]

#record the time that the udpating starts; will use this as the 'updated-to' time, overlapping a bit to cover time it takes to run the script itself
#formatting it to the way the api uses it from the output of python's datetime isoformat() function
upToDateTime = ((datetime.datetime.utcnow().isoformat())[:-7] + 'Z')

#pull last updated date
try:
    fp = open("most_recent_sherpaRomeo_update.txt", 'r', encoding='utf-8')
    prevUpdate = fp.read().rstrip()
    fp.close()
except FileNotFoundError:
    prevUpdate = None

def retrieve_from_api(objectType, outputDir):
    rootURL = "https://v2.sherpa.ac.uk/cgi/retrieve?"
    itemRequested = "item-type="+objectType
    dataFormat = "&format=Json"
    numItems = "&limit=100"
    pullPoint = 0
    orderTrait = "&order=date_modified"
    apiKey = "&api-key=B7EF093A-9FA5-11EC-ADDF-E2EE7BB5F954"

    if prevUpdate == None:
        filterURLString = ""
    else:
        #editing the API call to pull only the updated files by adding "filterString"
        #the odd characters in the search string get filtered, the [ becomes '%5B', " becomes '%22', etc. url encoding
        #"greater" means "later" in the context of the date modified filter here ('gte' means 'greater than')
        filterString = "[[\"date_modified\",\"gte\",\""+prevUpdate+"\"]]"
        filterURLString = "&filter="+urllib.parse.quote(filterString)

    #make it if it aint there already
    os.makedirs(outputDir, exist_ok=True)

    while True:
            completedURL = rootURL + itemRequested + dataFormat + numItems + "&offset=" + str(pullPoint) + orderTrait + filterURLString + apiKey
            outputLocation = outputDir + "/" + str(pullPoint) + ".json"

            print("Accessing " + completedURL, file=sys.stderr)

            #pull from the api
            response = requests.get(completedURL)
            payload = response.json()

            #break while loop if there's no payload
            if len(payload['items']) == 0:
                    break

            #else, just write to file and increment
            outputFile = open(outputLocation, "w", encoding='utf-8')

            print("Writing to " + outputLocation, file=sys.stderr)
            json.dump(payload['items'], outputFile, ensure_ascii=False, indent=4)

            #cleanup
            outputFile.close()

            #update offset, filename, and the api url
            pullPoint += 100

retrieve_from_api("publisher_policy", "apiout_pp")
retrieve_from_api("publication", "apiout_journal")
   
#update the 'last-updated' date
fp = open(last_update_file, 'w', encoding='utf-8')
fp.write(upToDateTime)
fp.close()
