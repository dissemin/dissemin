#!/usr/bin/env python3

#test file for the section of code that ingests new paper data from the crossref_import table

import django
import dissemin

django.setup()

import json

from backend.citeproc import Citeproc
from papers.baremodels import BareName, BareOaiRecord, BarePaper

#need some dissemin functions to find internal values
from publishers.models import Journal
from publishers.models import Publisher
from papers.models import OaiSource, Paper

#open up database connection (our database is on alexandra)
import os
import psycopg2
import sys

assert len(sys.argv) >= 4, "Usage: "+sys.argv[0]+" host database username password ..."

host = sys.argv[1]
database = sys.argv[2]
username = sys.argv[3]
password = sys.argv[4]

conn = psycopg2.connect(database=database, host=host,
        user=username, password=password)
#conn.set_session(autocommit=True) #removed for server-side cursor
cur = conn.cursor('cur') #giving a name makes the cursor server-sided instead of trying to download all the data locally

rCount = 0

# pulling data that we already have processed and stored in crossref_import into [bare_paper_data] and [bare_oairecord_data] objects so we can process them into a [paper] object

cur.execute(
    """
    SELECT * FROM crossref_import;
    """
)

crossref_identifier = OaiSource.objects.get(identifier='crossref')

for importRecord in cur:

    affiliaList = []    #list of affiliations
    namesList = []      #list of author names
    orcidList = []      #list of orcIDs

    #item [2] in the touple from the psychopg2 ordered object should be the 'authors' jsonb object
    #the 'authors' item is a [List] of {Dictionaries} so we need to iterate through and break it into three other lists
    tmp_list = importRecord[2]

    for a in range(len(tmp_list)):
        if "affiliation" in tmp_list[a]:
            affiliaList.append(tmp_list[a]["affiliation"])
        else:
            affiliaList.append(None)
        if "first" or "last" in tmp_list[a]:
            namesList.append(BareName.create_bare(tmp_list[a]["first"], tmp_list[a]["last"])) #<- this could be VERY stupid
        else:
            namesList.append(None)
        if "ORCID" in tmp_list[a]:
            orcidList.append(tmp_list[a]["ORCID"])
        elif "orcid" in tmp_list[a]:
            orcidList.append(tmp_list[a]["orcid"])
        else:
            orcidList.append(None)

    bare_paper_data = {
        'affiliations' : affiliaList,
        'author_names' : namesList,
        'orcids' : orcidList,
        'pubdate' : importRecord[4],
        'title' : importRecord[1],
    }

    journal = Journal.find(issn=importRecord[10], title=importRecord[9])
    publisher = Citeproc._get_publisher(importRecord[11], journal)

    bare_oairecord_data = {
        'doi' : importRecord[8],
        'description' : importRecord[15],
        'identifier' : importRecord[0],
        'issn' : importRecord[10],
        'issue' : importRecord[12],
        'journal' : journal,
        'journal_title' : importRecord[9],
        'pages' : importRecord[14],
        'pdf_url' : importRecord[6],
        'pubdate' : importRecord[4],
        'publisher' : publisher,
        'publisher_name' : importRecord[11],
        'pubtype' : importRecord[5], #doctype
        'source' : crossref_identifier,
        'splash_url' : importRecord[7],
        'volume' : importRecord[13],
        'keywords': importRecord[16]
    }

    try:
        bare_paper = BarePaper.create(**bare_paper_data)

        bare_oairecord = BareOaiRecord(paper=bare_paper, **bare_oairecord_data)

        oairecord = bare_paper.add_oairecord(bare_oairecord)
        oairecord.last_update = importRecord[3]
        bare_paper.update_availability()

        paper = Paper.from_bare(bare_paper)

        # Do not update Elastic Search index during looped ingestion,
        # wait for all entries to be added first, 
        # will improve efficiency
        #paper.update_index()
        rCount += 1
        if rCount % 1000 == 0:
            print("completed {} records".format(rCount))
    except:
        print("data error at DOI:" + importRecord[8])
