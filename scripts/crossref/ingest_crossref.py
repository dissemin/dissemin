#!/usr/bin/env python3

# Load packages from two level up as the script is located two
# subdirectories down from the Dissemin packages
import sys
from pathlib import Path
file = Path(__file__).resolve()
parent, top = file.parent, file.parents[2]
sys.path.append(str(top))
sys.path.remove(str(parent))

#import hashlib
import json
import gzip
import os
import psycopg2
from psycopg2.extras import execute_values, Json
import django
from html import unescape
import dissemin
print("Running dissemin code from %s"%(dissemin.settings.BASE_DIR))
django.setup()

from backend.citeproc import Citeproc
from papers.baremodels import BareName
from papers.utils import validate_orcid
from papers.doi import doi_to_crossref_identifier
from papers.doi import doi_to_url

assert len(sys.argv) >= 5, "Usage: "+sys.argv[0]+" host database username password gzipped_json_or_dir ..."

host = sys.argv[1]
database = sys.argv[2]
username = sys.argv[3]
password = sys.argv[4]

conn = psycopg2.connect(database=database, host=host,
        user=username, password=password)
conn.set_session(autocommit=True)
cur  = conn.cursor()

cur.execute(
    "SELECT EXISTS(SELECT * FROM information_schema.tables WHERE " +
    "table_name = 'crossref_import')")
if cur.fetchone()[0]:
    print("Table crossref_import already exists, appending",
          file=sys.stderr)
else:
    print("Creating table crossref_import")
    cur.execute(
        "CREATE TABLE crossref_import (" +
        "identifier varchar(512)," +
        "title varchar(1024)," +
        "authors jsonb," +
        "last_modified timestamp with time zone," +
        "pubdate date," +
        "doctype varchar(64)," +
        "pdf_url varchar(1024)," +
        "splash_url varchar(1024)," +
        "doi varchar(1024)," +
        "journal_title varchar(512)," +
        "journal_issn varchar(10)," +
        "publisher_name varchar(512)," +
        "issue varchar(64)," +
        "volume varchar(64)," +
        "pages varchar(64)," +
        "description text," +
        "keywords text)"
    )

nb_items = 0
nb_items_processed = 0

BATCH_SIZE = 1000
to_insert = []

def insert_data(cur, l):
    execute_values(cur, "INSERT INTO crossref_import VALUES %s", l,
                   page_size = BATCH_SIZE)

#md5sums=set()

for c in range(5, len(sys.argv)):
    files = sys.argv[c]

    if os.path.isdir(files):
        to_process = map(lambda f : files+"/"+f, sorted(
            os.listdir(files),
            key = lambda f : int(f.replace(".json.gz",""))))
    elif os.path.isfile(files):
        to_process = [files]
    else:
        print(files + " is neither a file nor a directory", file=sys.stderr)
        sys.exit(1)

    for f in to_process:
        print("Importing data from "+f)
        with gzip.open(f, "r") as g:
            data = g.read()
            j = json.loads(data.decode('utf-8'))
            items = j["items"]
            for i in items:
                nb_items+=1

                # CrossRef API spec is at
                # https://github.com/CrossRef/rest-api-doc/blob/master/api_format.md

                # We need to retrieve:
                # paper title, paper authors (with full/last/first name,
                # orcid id, affiliation, researcher id), last modification date,
                # publication date, doctypes (journal-article,
                # proceedings-article, thesis, other, book-chapter...), PDF
                # URL, Splash URL, DOI, journal title (+ ISSN, ESSN),
                # publisher name, issue, volume, pages, abstract,
                # keywords

                try:
                    title = i['title'][0]
                except:
                    # No title, cannot be processed
                    continue

                try:
                    authors = i['author']
                except:
                    # No authors, cannot be processed
                    continue

                authors_json = []
                for a in authors:
                    np = Citeproc._convert_to_name_pair(a)
                    if np == None:
                        # Unparsable name, cannot be processed
                        continue
                    (first, last) = np
                    js = BareName.create_bare(first, last).serialize()
                    if 'ORCID' in a:
                        js['orcid'] = validate_orcid(a['ORCID'])
                    if 'affiliation' in a and len(a['affiliation'])>0:
                        js['affiliation'] = Citeproc._get_affiliation(a)
                    authors_json.append(js)

                pubdate = Citeproc._get_pubdate(i)
                doi = Citeproc._get_doi(i)
                splash_url = doi_to_url(doi)
                licenses = i.get('licenses', [])
                pdf_url = Citeproc._get_pdf_url(doi, licenses, splash_url)
                if pdf_url == '':
                    if 'link' in i:
                        for l in i['link']:
                            if l['content-type'] == 'application/pdf':
                                pdf_url = l['URL']
                                break

                try:
                    journal_title = i['container-title']
                    if journal_title != None and len(journal_title)>0:
                        journal_title = unescape(journal_title[0])
                except:
                    # No journal name, cannot be processed
                    continue

                issn = Citeproc._get_issn(i)

                publisher_name = i.get('publisher', '')[:512]
                description =  Citeproc._get_abstract(i)
                identifier = doi_to_crossref_identifier(doi)
                issue =  i.get('issue', '')
                pages =  i.get('page', '')
                pubdate =  Citeproc._get_pubdate(i)
                pubtype =  Citeproc._get_pubtype(i)
                volume =  i.get('volume', '')
                doctype = Citeproc._get_pubtype(i)

                last_modified = Citeproc._parse_date(i['deposited'])
                if 'subject' in i:
                    keywords = ' | '.join(i['subject'])
                else:
                    keywords = None

                # m = hashlib.md5()
                # m.update(identifier.encode())
                # digest = m.digest()
                # if digest in md5sums:
                #     continue
                # else:
                #     md5sums.add(digest)

                to_insert.append([identifier, title[:1024], Json(authors_json),
                                  last_modified, pubdate, doctype,
                                  pdf_url[:1024], splash_url, doi, journal_title[:512],
                                  issn, publisher_name[:512], issue[:64], volume[:64],
                                  pages[:64], description, keywords
                                  ])
                nb_items_processed+=1

                if nb_items_processed % BATCH_SIZE == 0:
                    insert_data(cur,to_insert)
                    to_insert = []

if len(to_insert)>0:
    insert_data(cur, to_insert)
    to_insert = []

print("Seen %d items, processed %d items, skipped %d items"%(nb_items,nb_items_processed,nb_items-nb_items_processed))

cur.close()
conn.commit()
