#!/usr/bin/env python3

#include stuff as needed

#import django
#import dissemin

#django.setup()

import math

#open up database connection: open connection to database on alexandra (should we just hard code it?)
import os
import sys
import psycopg2

assert len(sys.argv) >= 4, "Usage: "+sys.argv[0]+" host database username password ..."

host = sys.argv[1]
database = sys.argv[2]
username = sys.argv[3]
password = sys.argv[4]

conn = psycopg2.connect(database=database, host=host,
        user=username, password=password)
conn.set_session(autocommit=True)
cur = conn.cursor()

# Create a temporary table which will assign offsets to each identifier
cur.execute(
    """
    CREATE TEMPORARY TABLE indexed_crossref_import AS SELECT ROW_NUMBER() OVER (ORDER BY identifier), identifier FROM crossref_import;
    """
)
cur.execute(
    """
    CREATE INDEX ON indexed_crossref_import(row_number)
    """
)

#the update is running into memory limits so we're breaking things into chunks
#first, count the amount of records in crossref
cur.execute(
    """
    SELECT max(row_number) FROM indexed_crossref_import;
    """
)

#then store the output in a python variable
crossrefRemnants = cur.fetchone()

batchSize = 100000
#round up so we get whatever remains
iterationCount = math.ceil(crossrefRemnants[0]/batchSize)
print(str(iterationCount) + "loops")

#do the updates in a loop so we can chunk them up
for Lnum in range(iterationCount):
    #update the papers_oairecord, then run the above delete/cleaning query after
    #use a chunk
    cur.execute(
        """
        UPDATE papers_oairecord
        SET
        pubdate = crossref_import.pubdate,
        pubtype = crossref_import.doctype,
        pdf_url = crossref_import.pdf_url,
        splash_url = crossref_import.splash_url,
        doi = crossref_import.doi,
        publisher_name = crossref_import.publisher_name,
        issue = crossref_import.issue,
        volume = crossref_import.volume,
        pages = crossref_import.pages,
        description = crossref_import.description,
        keywords = crossref_import.keywords,
        last_update = crossref_import.last_modified,
        journal_title = crossref_import.journal_title,
        journal_id = papers_journal.id,
        publisher_id = papers_journal.publisher_id
            FROM (SELECT crossref_import.*, row_number FROM crossref_import,indexed_crossref_import
                    WHERE crossref_import.identifier=indexed_crossref_import.identifier
                    AND row_number > {} AND row_number <= {} + {})
            crossref_import, papers_journal
        WHERE papers_oairecord.identifier = crossref_import.identifier
        AND papers_oairecord.last_update < crossref_import.last_modified
        AND (crossref_import.journal_issn in (papers_journal.issn, papers_journal.essn));
        """.format(Lnum*batchSize, Lnum*batchSize, batchSize)
    )
    print(str(cur.rowcount) + " records adjusted in loop #" + str(Lnum+1))
