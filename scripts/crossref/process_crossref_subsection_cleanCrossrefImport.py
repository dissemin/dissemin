#!/usr/bin/env python3

#include stuff as needed

#import django
#import dissemin

#django.setup()

import math

#open up database connection: open connection to database on alexandra (should we just hard code it?)
import os
import sys
import psycopg2

assert len(sys.argv) >= 4, "Usage: "+sys.argv[0]+" host database username password ..."

host = sys.argv[1]
database = sys.argv[2]
username = sys.argv[3]
password = sys.argv[4]

conn = psycopg2.connect(database=database, host=host,
        user=username, password=password)
conn.set_session(autocommit=True)
cur = conn.cursor()

# Create a temporary table which will assign offsets to each identifier
cur.execute(
    """
    CREATE TEMPORARY TABLE indexed_crossref_import AS SELECT ROW_NUMBER() OVER (ORDER BY identifier), identifier FROM crossref_import;
    """
)
cur.execute(
    """
    CREATE INDEX ON indexed_crossref_import(row_number);
    """
)

#the update is running into memory limits so we're breaking things into chunks
#first, count the amount of records in crossref
cur.execute(
    """
    SELECT max(row_number) FROM indexed_crossref_import;
    """
)

#then store the output in a python variable
crossrefRemnants = cur.fetchone()

batchSize = 100000
#round up so we get whatever remains
iterationCount = math.ceil(crossrefRemnants[0]/batchSize)
print(str(iterationCount) + "loops")

#do the updates in a loop so we can chunk them up
for Lnum in range(iterationCount):
    #update the papers_oairecord, then run the above delete/cleaning query after
    #use a chunk
    cur.execute(
        """
        DELETE FROM crossref_import
        USING indexed_crossref_import, papers_oairecord
        WHERE crossref_import.identifier = papers_oairecord.identifier
          AND crossref_import.identifier=indexed_crossref_import.identifier
          AND row_number > {} AND row_number <= {} + {}
        AND crossref_import.last_modified <= papers_oairecord.last_update ;
        """.format(Lnum*batchSize, Lnum*batchSize, batchSize)
    )
    print(str(cur.rowcount) + " records removed in loop #" + str(Lnum+1))
