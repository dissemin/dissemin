
Crossref Ingestion Scripts


Ingesting the latest Crossref data dump directly into the database was estimated to take too much time (~1 year)
	to speed this up we pull the latest dump data into the table crossref_import and address different data cases separately


ingest_crossref.py - creates the crossref_import table if it doesn't already exist, and then adds the data from a gzipped file to the crossref_import table

process_crossref_subsection_cleanCrossrefImport.py - deletes records from the crossref_import table that have the same or older 'last modified' value

process_crossref_subsection_updateOairecords.py - updates the papers_oairecords table with data from crossref_import that is newer

process_crossref_subsection_newPapers.py - ingests the rest of the valid data from the crossref_import table using existing dissemin functions



Usage/process:
	download a gzipped crossref dump locally

	run ingest_crossref.py with the crossref dump

	run process_crossref_subsection_cleanCrossrefImport.py

	run process_crossref_subsection_updateOairecords.py

	run process_crossref_subsection_cleanCrossrefImport.py

	run process_crossref_subsection_newPapers.py



Potential Improvements:
	in the newPapers script the psycopg2 connection uses a server-side pointer, and it would probably make the other scripts faster to also do that in them.
	creating a distinct script for updating data for the papers_paper table, since the crossref dump also contains data used there
	change the ingest_crossref.py script to create a "pubtype" field in the crossref_import table instead of "doctype". might just be a typo but 'doctype' doesn't seem to show up in the tables we're adjusting
	create a disctinct script for cleaning invalid data out of the crossref_import table. 

Notes:
	psycopg2 uses client side cursors and copying database data by default, 
		presumably because it allows more varied manipulation of the data to have it locally, 
		however our data is too large and we don't use any of the data manipulation that would require a client side cursor
	getting a nonzero amount of bad data (records without authors or titles) that could hypothetically be cleaned out of crossref_import before running the newPapers script
	the newPapers script takes the longest amount of time to run

